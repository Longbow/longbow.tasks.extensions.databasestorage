﻿// Copyright (c) Argo Zhang (argo@163.com). All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// Website: https://www.blazor.zone or https://argozhang.github.io/

using Longbow.Tasks;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace TasksEFCoreDemo
{
    public class TaskDemo : ITask
    {
        public Task Execute(CancellationToken cancellationToken)
        {
            Console.WriteLine(1233);
            return Task.CompletedTask;
        }
    }
}
