﻿// Copyright (c) Argo Zhang (argo@163.com). All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// Website: https://www.blazor.zone or https://argozhang.github.io/

using Longbow.Tasks;
using Longbow.Tasks.EntityFrameworkCore.Model;
using Longbow.Tasks.EntityFrameworkCoreStorage;
using Microsoft.Extensions.Logging;

namespace TasksEFCoreDemo
{
    /// <summary>
    /// 
    /// </summary>
    public class EFCoreStorageDemo : EFCoreStorage
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="logger"></param>
        public EFCoreStorageDemo(SchedulerDBContext dbContext, ILogger<EFCoreStorageDemo> logger) : base(dbContext, logger)
        {
        }

        protected override ITask CreateTaskByScheduleName(string scheduleName)
        {
            if (scheduleName == "TaskDemo")
            {
                return new TaskDemo();
            }
            else
            {
                return null;
            }
        }
    }
}
