﻿// Copyright (c) Argo Zhang (argo@163.com). All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// Website: https://www.blazor.zone or https://argozhang.github.io/

using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Longbow.Tasks.EntityFrameworkCore
{
    internal static class StringExtensions
    {
        internal static Dictionary<string, object>? ToDictionary(this string payload, ILogger logger)
        {
            Dictionary<string, object>? ret = null;
            try
            {
                ret = JsonSerializer.Deserialize<Dictionary<string, object>>(payload);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "反序列化失败");
            }
            return ret;
        }

        internal static string? ToJosnString(this Dictionary<string, object> keyValues)
        {
            return JsonSerializer.Serialize(keyValues);
        }
    }
}
